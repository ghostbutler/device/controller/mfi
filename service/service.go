package service

import (
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"gitlab.com/ghostbutler/tool/service/rule"
	"sync"
)

const (
	VersionMajor = 1
	VersionMinor = 0
)

type Service struct {
	// service basis
	service *common.Service

	// key manager
	keyManager *keystore.KeyManager

	// configuration
	configuration *Configuration

	// scanner rule auto adder
	ruleAdderManager *rule.RuleManager

	// controllers list
	controller map[string]*Controller

	// build directory manager
	directoryManager *common.DirectoryManager

	// rabbit mq controller
	rabbitController *rabbit.Controller

	// mutex
	sync.Mutex
}

// build the service
func BuildService(listeningPort int,
	configuration *Configuration) (*Service, error) {
	// allocate
	service := &Service{
		configuration: configuration,
		keyManager: keystore.BuildKeyManager(configuration.SecurityManager.Hostname,
			configuration.SecurityManager.Username,
			configuration.SecurityManager.Password,
			common.GhostService[common.ServiceControllerMFI].Name),
		controller: make(map[string]*Controller),
	}

	// rabbit mq controller
	var err error
	if service.rabbitController, err = rabbit.BuildController(configuration.RabbitMQ.Hostname,
		configuration.RabbitMQ.Username,
		configuration.RabbitMQ.Password); err != nil {
		return nil, err
	}

	// declare queues
	if _, err := service.rabbitController.Channel.QueueDeclare(rabbit.SensorQueueName,
		true,
		false,
		false,
		false,
		nil); err != nil {
		return nil, err
	}

	// build directory manager
	service.directoryManager = common.BuildDirectoryManager(configuration.Directory.Hostname,
		service.keyManager)

	// build rule manager
	service.ruleAdderManager = rule.BuildRuleManager(service.configuration.Scanner.Hostname,
		service.keyManager,
		[]string{DetectionRule})

	// build service basis
	service.service = common.BuildService(listeningPort,
		common.ServiceControllerMFI,
		APIService,
		VersionMajor,
		VersionMinor,
		nil,
		[]device.CapabilityType{device.CapabilityTypeOutlet},
		configuration.SecurityManager.Hostname,
		service)

	// ok
	return service, nil
}

// update service
func (service *Service) Update() {
	// lock mutex
	service.Lock()
	defer service.Unlock()

	// iterate controller
	for key, controller := range service.controller {
		// is controller running?
		if !controller.isRunning {
			// notify
			fmt.Println("removing controller for",
				controller.address)

			// remove from map
			delete(service.controller,
				key)
		}
	}
}

// enable/disable outlet
func (service *Service) SetEnable(name string,
	isEnabled bool) {
	// lock
	service.Lock()
	defer service.Unlock()

	// iterate
	for _, controller := range service.controller {
		controller.SetEnable(name,
			isEnabled)
	}
}

// enable/disable
func (service *Service) SetEnableAll(isEnabled bool) {
	// lock
	service.Lock()
	defer service.Unlock()

	// iterate
	for _, controller := range service.controller {
		controller.SetEnableAll(isEnabled)
	}
}

// test outlet
func (service *Service) Test(name string) {
	service.Lock()
	controllerList := make([]*Controller, 0, 1)
	for _, controller := range service.controller {
		controllerList = append(controllerList,
			controller)
	}
	service.Unlock()
	for _, controller := range controllerList {
		controller.Test(name)
	}
}
